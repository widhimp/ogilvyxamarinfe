﻿using System;
using System.Collections.Generic;
using OgilvyMusic.ViewModels;
using Xamarin.Forms;

namespace OgilvyMusic.Views
{
    public partial class SongList : ContentPage
    {
        public SongList()
        {
            InitializeComponent();

            BindingContext = new SongListViewModel();
        }
    }
}
