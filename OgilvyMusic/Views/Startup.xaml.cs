﻿using System;
using System.Collections.Generic;
using OgilvyMusic.ViewModels;
using Xamarin.Forms;

namespace OgilvyMusic.Views
{
    public partial class Startup : ContentPage
    {
        public Startup()
        {
            InitializeComponent();
            BindingContext = new StartupViewModel();
            NavigationPage.SetHasNavigationBar(this, false);
        }
    }
}
