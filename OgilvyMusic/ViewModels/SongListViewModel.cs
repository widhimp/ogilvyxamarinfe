﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Threading.Tasks;
using System.Windows.Input;
using OgilvyMusic.Models;
using Xamarin.Forms;

namespace OgilvyMusic.ViewModels
{
    public class SongListViewModel : BaseViewModel
    {
        private ObservableCollection<SongModel> _songList;
        public ObservableCollection<SongModel> SongList
        {
            get => _songList;
            set
            {
                _songList = value;
                OnPropertyChanged("SongList");
            }
        }

        public ICommand AddSongCommand { get; set; }

        public ObservableCollection<SongModel> dummySong;

        public SongListViewModel()
        {
            SongList = new ObservableCollection<SongModel>();
            SongList.Add(new SongModel
            {
                Artist = "Kangen Band",
                Song = "Kembali Pulang"
            });
            SongList.Add(new SongModel
            {
                Artist = "ILIR 7",
                Song = "Salah Apa Aku"
            });

            CreateDummySong();
            
            AddSongCommand = new Command(() => AddSong());
        }

        private void CreateDummySong()
        {
            dummySong = new ObservableCollection<SongModel>();
            dummySong.Add(new SongModel
            {
                Artist = "Mahen",
                Song = "Pura Pura Lupa"
            });
            dummySong.Add(new SongModel
            {
                Artist = "Andmesh",
                Song = "Nyaman"
            });
            dummySong.Add(new SongModel
            {
                Artist = "Feby Putri",
                Song = "Halu"
            });
            dummySong.Add(new SongModel
            {
                Artist = "Soundwave",
                Song = "Inikah Cinta"
            });
            dummySong.Add(new SongModel
            {
                Artist = "Brisia Jodie",
                Song = "Kisahku"
            });
            dummySong.Add(new SongModel
            {
                Artist = "Marion Jola",
                Song = "Rayu"
            });
            dummySong.Add(new SongModel
            {
                Artist = "RAN",
                Song = "Saling Merindu"
            });
            dummySong.Add(new SongModel
            {
                Artist = "Hivi!",
                Song = "Bumi dan Bulan"
            });
            dummySong.Add(new SongModel
            {
                Artist = "Rossa",
                Song = "Tegar"
            });
            dummySong.Add(new SongModel
            {
                Artist = "Pamungkas",
                Song = "One Only"
            });
        }

        private void AddSong()
        {
            Random rnd = new Random();
            SongList.Add(dummySong[rnd.Next(0,9)]);
        }
    }
}
