﻿using System;
using System.ComponentModel;
using System.Windows.Input;
using OgilvyMusic.Views;
using Xamarin.Forms;

namespace OgilvyMusic.ViewModels
{
    public class StartupViewModel : BaseViewModel
    {

        #region properties
        public ICommand GoToSongListCommand { get; set; }
        #endregion

        public StartupViewModel()
        {
            GoToSongListCommand = new Command(() => goToSongList());
        }

        private void goToSongList()
        {
            Application.Current.MainPage.Navigation.PushAsync(new SongList());
        }
    }
}
