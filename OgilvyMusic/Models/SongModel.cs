﻿using System;
namespace OgilvyMusic.Models
{
    public class SongModel
    {
        public string Artist { get; set; }
        public string Song { get; set; }

        public override string ToString()
        {
            return Artist + " - " + Song;
        }
    }
}
