﻿using System;
using OgilvyMusic.Views;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace OgilvyMusic
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();

            MainPage = new NavigationPage(new Startup());
        }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}
